
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;




public class Pratica42 {
    public static void main(String[] args) {
        Elipse a = new Elipse (5.0,3.7);
        Elipse b = new Elipse (3.0,2.5);
        
        Circulo c = new Circulo(8.0);
        Circulo d = new Circulo(9.5);
        
        System.out.println(a.getNome() + "Perímetro Primeira Elipse: "+ a.getPerimetro() + "Área da Primeira Elipse: "+a.getArea() );
        System.out.println(b.getNome() + "Perímetro Segunda Elipse: "+ b.getPerimetro() + "Área da Segunda Elipse: "+b.getArea() );
        System.out.println(c.getNome() + "Perímetro Primeiro Circulo: "+ c.getPerimetro() + "Área do Primeiro Circulo: "+ c.getArea() );
        System.out.println(d.getNome() + "Perímetro Segundo Circulo: "+ d.getPerimetro() + "Área do Segundo Circulo: "+ d.getArea() );
    }
}
