/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;

/**
 *
 * @author Ane Carolina Simões
 */
public class Elipse implements FiguraComEixos {
    
    protected double r;
    protected double s;
    
    public Elipse() {}
    
    public Elipse(double r, double s) {
        this.r = r;
        this.s = s;
    }
    
    public double getArea()
    {
        return Math.PI*this.r*this.s;
    }
    
    public double getPerimetro()
    {
        return PI*(3*(r+s)-sqrt((3*r+s)*(r+3*s)));
    }

    @Override
    public String getNome() {
        return this.getClass().getSimpleName();
    }

    @Override
    public double getEixoMenor() {
        if(r<s)
            return r;
        else
            return s;
    }

    @Override
    public double getEixoMaior() {
        if(r>s)
            return r;
        else
            return s;
    }
    
    
    
    
}

